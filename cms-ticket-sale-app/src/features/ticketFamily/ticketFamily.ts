import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { collection, doc, setDoc, getFirestore } from 'firebase/firestore';
import { TicketFamily } from '../../types/TicketFamily';
import {
    app, db
} from "../../lib/firebase";
interface TicketFamilyState {
    loading: boolean;
    error: string | null;
}

const initialState: TicketFamilyState = {
    loading: false,
    error: null,
};

export const firestore = getFirestore(app);

export const ticketFamilyCollection = collection(firestore, 'ticketFamily');

export const updateTicketFamily = createAsyncThunk<void, { id: string | undefined, docData: TicketFamily }, { state: RootState }>(
    'ticketManagementContent/updateTicketFamily',
    async ({ id, docData }, { rejectWithValue }) => {
        try {
            const getTicketFamily = doc(firestore, `ticketFamily/${id}`);
            await setDoc(getTicketFamily, docData, { merge: true });
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

const ticketFamilySlice = createSlice({
    name: 'ticketFamily',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(updateTicketFamily.pending, (state) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(updateTicketFamily.fulfilled, (state) => {
                state.loading = false;
                state.error = null;
            })
            .addCase(updateTicketFamily.rejected, (state, action) => {
                state.loading = false;
                state.error = action.payload as string;
            });
    },
});

export default ticketFamilySlice.reducer;