import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { collection, doc, setDoc, getFirestore } from 'firebase/firestore';
import { ServicePack } from '../../types/ServicePack';
import {
    app, db
} from "../../lib/firebase";
interface ServicePackState {
    loading: boolean;
    error: string | null;
}

const initialState: ServicePackState = {
    loading: false,
    error: null,
};

export const firestore = getFirestore(app);

export const servicePackCollection = collection(firestore, 'service-pack');

export const addServicePack = createAsyncThunk<void, ServicePack, { state: RootState }>(
    'servicePack/addServicePack',
    async (servicePackData, { rejectWithValue }) => {
        try {
            const newServicePack = await setDoc(doc(servicePackCollection), servicePackData);
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

export const updateServicePack = createAsyncThunk<void, { id: string | undefined, docData: ServicePack }, { state: RootState }>(
    'servicePack/updateServicePack',
    async ({ id, docData }, { rejectWithValue }) => {
        try {
            const getServicePack = doc(firestore, `service-pack/${id}`);
            await setDoc(getServicePack, docData, { merge: true });
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

const servicePackSlice = createSlice({
    name: 'servicePack',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(addServicePack.pending, (state) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(addServicePack.fulfilled, (state) => {
                state.loading = false;
                state.error = null;
            })
            .addCase(addServicePack.rejected, (state, action) => {
                state.loading = false;
                state.error = action.payload as string;
            })
            .addCase(updateServicePack.pending, (state) => {
                state.loading = true;
                state.error = null;
            })
            .addCase(updateServicePack.fulfilled, (state) => {
                state.loading = false;
                state.error = null;
            })
            .addCase(updateServicePack.rejected, (state, action) => {
                state.loading = false;
                state.error = action.payload as string;
            });
    },
});

export default servicePackSlice.reducer;