import React from 'react'
import Sider from "antd/es/layout/Sider";
import {
    Button,
    Image,
    Menu,
    Typography
} from 'antd';
import {
    HomeOutlined,
    TagOutlined,
    BarsOutlined,
    TagsOutlined,
    LaptopOutlined,
    SettingOutlined
} from '@ant-design/icons';
import { Link, useNavigate } from 'react-router-dom';
import MenuItem from 'antd/es/menu/MenuItem';
function SideBar() {
    const navigate = useNavigate();
    const { SubMenu, Item } = Menu;
    return (
        <Sider
            width={210}
            trigger={null}
            style={{
                minHeight: '100vh',
                backgroundColor: '#f5f5f5',
                padding: '17px'
            }}
        >
            <div
                style={{
                    display: 'flex',
                }}>
                <Image
                    width={100}
                    preview={false}
                    src='https://cdn.discordapp.com/attachments/1029747761094070354/1113516904095682791/image.png'
                    style={{
                        margin: '0 auto'
                    }} />
            </div>
            <Menu
                openKeys={['service-pack']}
                mode='inline'
                style={{
                    padding: '32px 0',
                    backgroundColor: '#f5f5f5'
                }}>
                <Menu.Item
                    style={{
                        borderRadius: '8px',
                        margin: 0,
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center',
                    }}>
                    <Link
                        to={`/`}
                    >
                        <HomeOutlined /> Trang chủ
                    </Link>
                </Menu.Item>
                <Menu.Item
                    style={{
                        borderRadius: '8px',
                        margin: 0,
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center'
                    }}>
                    <Link
                        to={`/ticket-management`}
                    >
                        <TagOutlined /> Quản lý vé
                    </Link>
                </Menu.Item>
                <Menu.Item
                    style={{
                        borderRadius: '8px',
                        margin: 0,
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center'
                    }}>
                    <Link
                        to={`/ticket-check`}
                    >
                        <TagsOutlined /> Đối soát vé
                    </Link>
                </Menu.Item>
                <Menu.Item
                    style={{
                        borderRadius: '8px',
                        margin: 0,
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center'
                    }}>
                    <Link
                        to={`/event-list`}
                    >
                        <BarsOutlined /> Danh sách sự kiện
                    </Link>
                </Menu.Item>
                <Menu.Item
                    style={{
                        borderRadius: '8px',
                        margin: 0,
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center'
                    }}>
                    <Link
                        to={`/equipment-management`}
                    >
                        <LaptopOutlined /> Quản lý thiết bị
                    </Link>
                </Menu.Item>
                <SubMenu
                    key="service-pack"
                    icon={<SettingOutlined style={{ color: '#7E7D88' }} />}
                    title="Cài đặt"
                    style={{
                        borderRadius: '8px',
                        margin: 0,
                        width: '100%',
                        alignItems: 'center',
                    }}
                >
                    <Item key="packet-service-submenu" >
                        <Link to="/service-pack">Gói dịch vụ</Link>
                    </Item>
                </SubMenu>
            </Menu>
        </Sider>
    )
}

export default SideBar