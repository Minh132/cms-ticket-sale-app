export interface TicketFamily {
    stt?: number;
    id?: string;
    bookingCode?: string;
    number?: number;
    status?: string;
    dateUse?: string;
    dateExport?: string;
    checkIn?: string;
    name?: string;
    checked?: string;
}