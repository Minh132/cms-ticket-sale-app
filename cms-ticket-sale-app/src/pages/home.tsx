import { Layout, Space } from 'antd'
import React from 'react'
import SideBar from '../components/SideBar'
import HomeContent from '../components/HomeContent'
import Header from '../components/Header'

function home() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <HomeContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default home