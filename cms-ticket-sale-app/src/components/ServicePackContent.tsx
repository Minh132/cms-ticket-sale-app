import {
    Button,
    Input,
    Modal,
    Typography,
    Table,
    DatePicker,
    TimePicker,
    Checkbox,
    Select
} from 'antd'
import React, {
    useState,
    useEffect
} from 'react'
import {
    SearchOutlined,
    EditOutlined,
    CalendarOutlined,
    ClockCircleOutlined,
    CaretDownOutlined
} from '@ant-design/icons';
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import {
    servicePackCollection,
    addServicePack,
    updateServicePack
} from '../features/servicePack/servicePack';
import { useDispatch } from 'react-redux';
import { ServicePack } from '../types/ServicePack';
import dayjs, { Dayjs } from 'dayjs';
function ServicePackContent() {

    const [servicePacks, setServicePacks] = useState<ServicePack[]>([]);
    const [name, setName] = useState("");
    const [code, setCode] = useState("");
    const [dayStart, setDayStart] = useState("");
    const [dayEnd, setDayEnd] = useState("");
    const [selectedDateStart, setSelectedDateStart] = useState<Dayjs | null>(null);
    const [selectedTimeStart, setSelectedTimeStart] = useState<Dayjs | null>(null);
    const [selectedDateEnd, setSelectedDateEnd] = useState<Dayjs | null>(null);
    const [selectedTimeEnd, setSelectedTimeEnd] = useState<Dayjs | null>(null);
    const [cost, setCost] = useState("");
    const [combo, setCombo] = useState("");
    const [isCostChecked, setIsCostChecked] = useState(false);
    const [isComboChecked, setIsComboChecked] = useState(false);
    const [status, setStatus] = useState("Đang áp dụng");
    const [stt, setStt] = useState(1);

    const [nameUpdate, setNameUpdate] = useState("");
    const [idUpdate, setIdUpdate] = useState("");
    const [codeUpdate, setCodeUpdate] = useState("");
    const [dayStartUpdate, setDayStartUpdate] = useState("");
    const [dayEndUpdate, setDayEndUpdate] = useState("");
    const [selectedDateStartUpdate, setSelectedDateStartUpdate] = useState<Dayjs | null>(null);
    const [selectedTimeStartUpdate, setSelectedTimeStartUpdate] = useState<Dayjs | null>(null);
    const [selectedDateEndUpdate, setSelectedDateEndUpdate] = useState<Dayjs | null>(null);
    const [selectedTimeEndUpdate, setSelectedTimeEndUpdate] = useState<Dayjs | null>(null);
    const [costUpdate, setCostUpdate] = useState("");
    const [comboUpdate, setComboUpdate] = useState("");
    const [isCostCheckedUpdate, setIsCostCheckedUpdate] = useState(false);
    const [isComboCheckedUpdate, setIsComboCheckedUpdate] = useState(false);
    const [statusUpdate, setStatusUpdate] = useState("Đang áp dụng");

    const [searchKeyword, setSearchKeyword] = useState<string>('');
    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchKeyword(event.target.value);
    };
    const handleUpdate = (id: any) => {
        setIdUpdate(id)
        onChangeFilterTicketsUpdate()
        console.log("Đã nhấp vào cập nhật cho ID gói:", id);
    };
    const handleCostCheckboxChange = (event: any) => {
        setIsCostChecked(event.target.checked);
    };
    const handleCostCheckboxChangeUpdate = (event: any) => {
        setIsCostCheckedUpdate(event.target.checked);
    };
    const handleComboCheckboxChange = (event: any) => {
        setIsComboChecked(event.target.checked);
    };
    const handleComboCheckboxChangeUpdate = (event: any) => {
        setIsComboCheckedUpdate(event.target.checked);
    };
    const handleSelectStatusChange = (value: string) => {
        setStatus(value);
    };
    const handleSelectStatusChangeUpdate = (value: string) => {
        setStatusUpdate(value);
    };
    const handleDateStartChange = (date: Dayjs | null) => {
        setSelectedDateStart(date);
    };
    const handleDateStartChangeUpdate = (date: Dayjs | null) => {
        setSelectedDateStartUpdate(date);
    };
    const handleTimeStartChange = (time: Dayjs | null) => {
        setSelectedTimeStart(time);
    };
    const handleTimeStartChangeUpdate = (time: Dayjs | null) => {
        setSelectedTimeStartUpdate(time);
    };
    const handleDateEndChange = (date: Dayjs | null) => {
        setSelectedDateEnd(date);
    };
    const handleDateEndChangeUpdate = (date: Dayjs | null) => {
        setSelectedDateEndUpdate(date);
    };
    const handleTimeEndChange = (time: Dayjs | null) => {
        setSelectedTimeEnd(time);
    };
    const handleTimeEndChangeUpdate = (time: Dayjs | null) => {
        setSelectedTimeEndUpdate(time);
    };
    const [popup, setPopup] = useState(false)
    const [popupUpdate, setPopupUpdate] = useState(false)
    const popupChange = () => {
        setPopup(true)
    }
    const popupChangeUpdate = () => {
        setPopupUpdate(true)
    }
    const onChangeFilterTickets = () => {
        popupChange()
        console.log("Popup showing");
    };
    const onChangeFilterTicketsUpdate = () => {
        popupChangeUpdate()
        console.log("Popup update showing");
    };
    const handleCancel = () => {
        setPopup(false);
    };
    const handleCancelUpdate = () => {
        setPopupUpdate(false);
    };
    const dispatch = useDispatch();

    const addNewServicePack = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            await dispatch(
                addServicePack({
                    stt,
                    code,
                    name,
                    dayStart,
                    dayEnd,
                    cost: isCostChecked || isComboChecked ? cost : "",
                    combo: isComboChecked ? combo : "",
                    status
                }) as any
            );
            console.log('successfully added a new device');
        } catch (error) {
            console.log('error:', error);
        }

        // Reset selected date and time for start date
        setSelectedDateStart(null);
        setSelectedTimeStart(null);

        // Reset selected date and time for end date
        setSelectedDateEnd(null);
        setSelectedTimeEnd(null);
    };
    const handleUpdateServicePack = async () => {
        try {
            await dispatch(
                updateServicePack({
                    id: idUpdate,
                    docData: {
                        code: codeUpdate,
                        name: nameUpdate,
                        dayStart: dayStartUpdate,
                        dayEnd: dayEndUpdate,
                        cost: isCostCheckedUpdate || isComboCheckedUpdate ? costUpdate : "",
                        combo: isComboCheckedUpdate ? comboUpdate : "",
                        status: statusUpdate,
                    }
                }) as any
            );
            console.log('successfully update a new Service');
        } catch (error) {
            console.log('error:', error);
        }
        // Reset selected date and time for start date
        setSelectedDateStartUpdate(null);
        setSelectedTimeStartUpdate(null);

        // Reset selected date and time for end date
        setSelectedDateEndUpdate(null);
        setSelectedTimeEndUpdate(null);
    };
    useEffect(() => {
        const unsubscribe = onSnapshot(servicePackCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const servicePackData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setServicePacks(servicePackData);
        });
        return () => {
            unsubscribe();
        };
    }, []);
    useEffect(
        () => {
            const dateStartString = selectedDateStart ? selectedDateStart.format('DD/MM/YYYY') : '';
            const timeStartString = selectedTimeStart ? selectedTimeStart.format('HH:mm:ss') : '';
            const dateTimeStartString = `${dateStartString} ${timeStartString}`;
            setDayStart(dateTimeStartString);
            const dateEndString = selectedDateEnd ? selectedDateEnd.format('DD/MM/YYYY') : '';
            const timeEndString = selectedTimeEnd ? selectedTimeEnd.format('HH:mm:ss') : '';
            const dateTimeEndString = `${dateEndString} ${timeEndString}`;
            setDayEnd(dateTimeEndString);
            if (!stt) {
                const maxStt = Math.max(...servicePacks.map((servicePack) => Number(servicePack.stt)), 0);
                setStt((maxStt + 1));
                console.log(stt)
            } else {
                const maxStt = Math.max(...servicePacks.map((servicePack) => Number(servicePack.stt)), 0);
                setStt((maxStt + 1));
                console.log(stt)
            }
        },
        [addNewServicePack]
    )
    useEffect(
        () => {
            const dateStartStringUpdate = selectedDateStartUpdate ? selectedDateStartUpdate.format('DD/MM/YYYY') : '';
            const timeStartStringUpdate = selectedTimeStartUpdate ? selectedTimeStartUpdate.format('HH:mm:ss') : '';
            const dateTimeStartStringUpdate = `${dateStartStringUpdate} ${timeStartStringUpdate}`;
            setDayStartUpdate(dateTimeStartStringUpdate);
            const dateEndStringUpdate = selectedDateEndUpdate ? selectedDateEndUpdate.format('DD/MM/YYYY') : '';
            const timeEndStringUpdate = selectedTimeEndUpdate ? selectedTimeEndUpdate.format('HH:mm:ss') : '';
            const dateTimeEndStringUpdate = `${dateEndStringUpdate} ${timeEndStringUpdate}`;
            setDayEndUpdate(dateTimeEndStringUpdate);
            // if (!sttUpdate) {
            //     const maxSttUpdate = Math.max(...servicePacks.map((servicePack) => Number(servicePack.stt)), 0);
            //     setSttUpdate((maxSttUpdate + 1));
            //     console.log(sttUpdate)
            // } else {
            //     const maxSttUpdate = Math.max(...servicePacks.map((servicePack) => Number(servicePack.stt)), 0);
            //     setSttUpdate((maxSttUpdate + 1));
            //     console.log(stt)
            // }
        },
        [handleUpdateServicePack]
    )
    const columns = [
        {
            title: 'STT',
            dataIndex: 'stt',
            key: 'stt',
            render: (stt: number) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {stt}
                    </Typography>
                );
            },
        },
        {
            title: 'Mã gói',
            dataIndex: 'code',
            key: 'code',
            render: (code: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {code}
                    </Typography>
                );
            },
        },
        {
            title: 'Tên gói vé',
            dataIndex: 'name',
            key: 'name',
            render: (name: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {name}
                    </Typography>
                );
            },
        },
        {
            title: 'Ngày áp dụng',
            dataIndex: 'dayStart',
            key: 'dayStart',
            render: (dayStart: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {dayStart}
                    </Typography>
                );
            },
        },
        {
            title: 'Ngày hết hạn',
            dataIndex: 'dayEnd',
            key: 'dayEnd',
            render: (dayEnd: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {dayEnd}
                    </Typography>
                );
            },
        },
        {
            title: 'Giá vé (VNĐ/Combo)',
            dataIndex: 'cost',
            key: 'cost',
            render: (cost: string) => {
                return (
                    <>
                        {cost === ''
                            ?
                            <Typography
                                style={{
                                    fontFamily: 'Montserrat-Medium',
                                    fontSize: '14px'
                                }}>
                                {cost}
                            </Typography>

                            :
                            <Typography
                                style={{
                                    fontFamily: 'Montserrat-Medium',
                                    fontSize: '14px'
                                }}>
                                {cost} VNĐ
                            </Typography>
                        }
                    </>
                );
            },
        },
        {
            title: 'Giá Combo (VNĐ/Combo)',
            dataIndex: 'combo',
            key: 'combo',
            render: (combo: string) => {
                return (
                    <>
                        {combo === ''
                            ?
                            <Typography
                                style={{
                                    fontFamily: 'Montserrat-Medium',
                                    fontSize: '14px'
                                }}>
                                {combo}
                            </Typography>

                            :
                            <Typography
                                style={{
                                    fontFamily: 'Montserrat-Medium',
                                    fontSize: '14px'
                                }}>
                                {combo} VNĐ
                            </Typography>
                        }
                    </>
                );
            },
        },
        {
            title: 'Tình trạng',
            dataIndex: 'status',
            key: 'status',
            render: (status: string) => {
                return (
                    <>
                        {status === 'Đang áp dụng'
                            ?
                            <div
                                style={{
                                    display: 'inline-flex',
                                    alignItems: 'center',
                                    backgroundColor: '#DEF7E0',
                                    border: '1px solid #03AC00',
                                    borderRadius: '4px',
                                    paddingInline: '8px',
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#03AC00',
                                        borderRadius: '100%',
                                        marginRight: '4px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Montserrat-Medium',
                                        fontSize: '14px',
                                        color: '#03AC00'
                                    }}>
                                    Đang áp dụng
                                </Typography>
                            </div>
                            : ""
                        }
                        {
                            status === 'Tắt'
                                ?
                                <div
                                    style={{
                                        display: 'inline-flex',
                                        alignItems: 'center',
                                        backgroundColor: '#F8EBE8',
                                        border: '1px solid #FD5959',
                                        borderRadius: '4px',
                                        paddingInline: '8px',
                                    }}>
                                    <div
                                        style={{
                                            width: '6px',
                                            height: '6px',
                                            backgroundColor: '#FD5959',
                                            borderRadius: '100%',
                                            marginRight: '4px'
                                        }}></div>
                                    <Typography
                                        style={{
                                            fontFamily: 'Montserrat-Medium',
                                            fontSize: '14px',
                                            color: '#FD5959'
                                        }}>
                                        Tắt
                                    </Typography>
                                </div>
                                : ""
                        }
                    </>
                );
            },
        },
        {
            title: ' ',
            dataIndex: 'update',
            key: 'update',
            render: (_: any, record: ServicePack) => {
                const { id } = record; // Giả sử ID của gói được lưu trữ trong thuộc tính 'id'

                return (
                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                        }}
                        onClick={() => handleUpdate(id)} // Truyền ID của gói vào hàm handleUpdate
                    >
                        <EditOutlined
                            style={{
                                color: '#FF993C'
                            }}
                        />
                        <Typography
                            style={{
                                fontFamily: 'Nunito-Regular',
                                fontSize: '14px',
                                color: '#FF993C'
                            }}
                        >
                            Cập nhật
                        </Typography>
                    </div>
                );
            },
        },
    ]
    return (
        <div
            style={{
                display: 'flex',
            }}>
            <div
                style={{
                    margin: '0 20px',
                }}>
                <div
                    style={{
                        minHeight: '660px',
                        maxHeight: '660px',
                        borderRadius: '12px',
                        backgroundColor: '#FFFFFF',
                        padding: '24px',
                    }}>
                    <div
                        style={{
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            width: '1240px',
                        }}>
                        <div>
                            <div>
                                <Typography
                                    style={{
                                        fontSize: '36px',
                                        fontFamily: 'Montserrat-Bold',
                                        color: '#1E0D03',
                                        lineHeight: 1
                                    }}>
                                    Danh sách gói vé
                                </Typography>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    width: '100%',
                                    alignItems: 'center',
                                }}>
                                <div>
                                    <Input
                                        onChange={handleSearch}
                                        className="custom-search-ticket"
                                        placeholder="Tìm bằng mã gói"
                                        style={{
                                            width: '360px',
                                            height: '36px',
                                            background: '#F7F7F8'
                                        }}
                                        suffix={
                                            <SearchOutlined
                                                style={{
                                                    fontSize: '18px'
                                                }}
                                            />}
                                    />
                                </div>
                                <div
                                    style={{
                                        display: 'flex',
                                    }}>
                                    <Button
                                        style={{
                                            height: '36px',
                                            borderRadius: '8px',
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginInline: '4px',
                                            borderColor: '#FF993C'
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '18px',
                                                fontFamily: 'Montserrat-Bold',
                                                color: '#FF993C'
                                            }}>
                                            Xuất file (.csv)
                                        </Typography>
                                    </Button>
                                    <Button
                                        onClick={() => onChangeFilterTickets()}
                                        style={{
                                            height: '36px',
                                            borderRadius: '8px',
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginInline: '4px',
                                            backgroundColor: '#FF993C'
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '18px',
                                                fontFamily: 'Montserrat-Bold',
                                                color: '#FFFFFF',
                                            }}>
                                            Thêm gói vé
                                        </Typography>
                                    </Button>
                                    <Modal
                                        centered
                                        onCancel={handleCancel}
                                        width={600}
                                        closable={false}
                                        bodyStyle={{
                                            height: '363px',
                                            flexDirection: 'column',
                                            justifyContent: 'space-between',
                                            alignItems: 'center'
                                        }}
                                        title={
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'center',
                                                }}>
                                                <Typography
                                                    style={{
                                                        fontSize: '24px',
                                                        fontFamily: 'Montserrat-Bold',
                                                        color: '#1E0D03'
                                                    }}>
                                                    Thêm gói vé
                                                </Typography>
                                            </div>
                                        }
                                        open={popup}
                                        footer={[
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'center'
                                                }}>
                                                <Button
                                                    style={{
                                                        paddingInline: '50px',
                                                        height: '36px',
                                                        borderRadius: '8px',
                                                        display: 'flex',
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        borderColor: '#FF993C',
                                                        textAlign: 'center',
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '18px',
                                                            fontFamily: 'Montserrat-Bold',
                                                            color: '#FF993C'
                                                        }}>
                                                        Huỷ
                                                    </Typography>
                                                </Button>
                                                <Button
                                                    onClick={(e: any) => addNewServicePack(e)}
                                                    style={{
                                                        paddingInline: '50px',
                                                        height: '36px',
                                                        borderRadius: '8px',
                                                        display: 'flex',
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        backgroundColor: '#FF993C',
                                                        textAlign: 'center',
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '18px',
                                                            fontFamily: 'Montserrat-Bold',
                                                            color: '#FFFFFF'
                                                        }}>
                                                        Lưu
                                                    </Typography>
                                                </Button>
                                            </div>
                                        ]}
                                    >
                                        <div>
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'space-between',
                                                    marginBottom: '12px',
                                                }}>
                                                <div>
                                                    <div
                                                        style={{
                                                            display: 'flex',
                                                        }}>
                                                        <Typography
                                                            style={{
                                                                fontSize: '16px',
                                                                fontFamily: 'Montserrat-SemiBold'
                                                            }}>
                                                            Mã sự kiện
                                                        </Typography>
                                                        <div
                                                            style={{
                                                                width: '25px',
                                                                height: '25px'
                                                            }}
                                                        >
                                                            <Typography
                                                                style={{
                                                                    color: 'red',
                                                                    fontSize: '16px',
                                                                    alignItems: 'center',
                                                                    display: 'flex',
                                                                    height: '20px',
                                                                    marginLeft: '2px'
                                                                }}>
                                                                *
                                                            </Typography>
                                                        </div>
                                                    </div>
                                                    <Input
                                                        onChange={(e) => setCode(e.target.value)}
                                                        placeholder="Nhập mã sự kiện"
                                                        style={{
                                                            width: '194px',
                                                            height: '32px',
                                                            fontSize: '16px'
                                                        }}
                                                    />
                                                </div>
                                                <div
                                                    style={{
                                                        width: '290px'
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold'
                                                        }}>
                                                        Tên sự kiện
                                                    </Typography>
                                                    <Input
                                                        onChange={(e) => setName(e.target.value)}
                                                        placeholder="Nhập tên sự kiện"
                                                        style={{
                                                            width: '290px',
                                                            height: '32px',
                                                            fontSize: '16px'
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'space-between',
                                                    marginBottom: '12px',
                                                }}>
                                                <div>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold'
                                                        }}>
                                                        Ngày áp dụng
                                                    </Typography>
                                                    <div
                                                        style={{
                                                            display: 'flex',
                                                        }}>
                                                        <DatePicker
                                                            placeholder='dd/mm/yy'
                                                            suffixIcon={<CalendarOutlined
                                                                style={{
                                                                    color: '#FF7506'
                                                                }} />}
                                                            value={selectedDateStart}
                                                            onChange={handleDateStartChange}
                                                            style={{
                                                                width: '113px',
                                                                height: '32px'
                                                            }}
                                                        />
                                                        <TimePicker
                                                            placeholder='hh:mm:ss'
                                                            suffixIcon={<ClockCircleOutlined
                                                                style={{
                                                                    color: '#FF7506'
                                                                }} />}
                                                            value={selectedTimeStart}
                                                            onChange={handleTimeStartChange}
                                                            style={{
                                                                width: '108px',
                                                                height: '32px'
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                                <div
                                                    style={{
                                                        width: '290px'
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold'
                                                        }}>
                                                        Ngày hết hạn
                                                    </Typography>
                                                    <div
                                                        style={{
                                                            display: 'flex',
                                                        }}>
                                                        <DatePicker
                                                            placeholder='dd/mm/yy'
                                                            suffixIcon={<CalendarOutlined
                                                                style={{
                                                                    color: '#FF7506'
                                                                }} />}
                                                            value={selectedDateEnd}
                                                            onChange={handleDateEndChange}
                                                            style={{
                                                                width: '113px',
                                                                height: '32px'
                                                            }}
                                                        />
                                                        <TimePicker
                                                            placeholder='hh:mm:ss'
                                                            suffixIcon={<ClockCircleOutlined
                                                                style={{
                                                                    color: '#FF7506'
                                                                }} />}
                                                            value={selectedTimeEnd}
                                                            onChange={handleTimeEndChange}
                                                            style={{
                                                                width: '108px',
                                                                height: '32px'
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <Typography
                                                    style={{
                                                        fontSize: '16px',
                                                        fontFamily: 'Montserrat-SemiBold'
                                                    }}>
                                                    Giá vé áp dụng
                                                </Typography>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        alignItems: 'center',
                                                        margin: '8px 0'
                                                    }}>
                                                    <Checkbox
                                                        checked={isCostChecked}
                                                        onChange={handleCostCheckboxChange}
                                                    >
                                                        <Typography
                                                            style={{
                                                                fontFamily: 'Montserrat-Medium',
                                                                fontSize: '16px',
                                                            }}
                                                        >
                                                            Vé lẻ (vnđ/vé) với giá
                                                        </Typography>
                                                    </Checkbox>
                                                    <Input
                                                        placeholder='Giá vé'
                                                        onChange={(e) => setCost(e.target.value)}
                                                        style={{
                                                            width: '117px',
                                                            height: '32px',
                                                            fontFamily: 'Nunito-Regular',
                                                            fontSize: '16px',
                                                            backgroundColor: '#F1F4F8'
                                                        }}>

                                                    </Input>
                                                    <div>
                                                        <Typography
                                                            style={{
                                                                fontFamily: 'Montserrat-Medium',
                                                                fontSize: '16px',
                                                                marginInline: '8px'
                                                            }}
                                                        >
                                                            / vé
                                                        </Typography>
                                                    </div>
                                                </div>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        alignItems: 'center',
                                                        margin: '8px 0'
                                                    }}>
                                                    <Checkbox
                                                        checked={isComboChecked}
                                                        onChange={handleComboCheckboxChange}
                                                    >
                                                        <Typography
                                                            style={{
                                                                fontFamily: 'Montserrat-Medium',
                                                                fontSize: '16px',
                                                            }}
                                                        >
                                                            Combo vé với giá
                                                        </Typography>
                                                    </Checkbox>
                                                    <Input
                                                        onChange={(e) => setCombo(e.target.value)}
                                                        placeholder='Giá vé'
                                                        style={{
                                                            width: '117px',
                                                            height: '32px',
                                                            fontFamily: 'Nunito-Regular',
                                                            fontSize: '16px',
                                                            backgroundColor: '#F1F4F8'
                                                        }}>

                                                    </Input>
                                                    <div>
                                                        <Typography
                                                            style={{
                                                                fontFamily: 'Montserrat-Medium',
                                                                fontSize: '16px',
                                                                marginInline: '8px'
                                                            }}
                                                        >
                                                            /
                                                        </Typography>
                                                    </div>
                                                    <Input
                                                        onChange={(e) => setCost(e.target.value)}
                                                        placeholder='Giá vé'
                                                        style={{
                                                            width: '117px',
                                                            height: '32px',
                                                            fontFamily: 'Nunito-Regular',
                                                            fontSize: '16px',
                                                            backgroundColor: '#F1F4F8'
                                                        }}>

                                                    </Input>
                                                    <div>
                                                        <Typography
                                                            style={{
                                                                fontFamily: 'Montserrat-Medium',
                                                                fontSize: '16px',
                                                                marginInline: '8px'
                                                            }}
                                                        >
                                                            vé
                                                        </Typography>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                style={{
                                                    marginBottom: '12px',
                                                }}>
                                                <Typography
                                                    style={{
                                                        fontSize: '16px',
                                                        fontFamily: 'Montserrat-SemiBold'
                                                    }}>
                                                    Tình trạng
                                                </Typography>
                                                <Select
                                                    value={status}
                                                    listHeight={180}
                                                    placeholder={'Chọn loại thiết bi'}
                                                    onChange={handleSelectStatusChange}
                                                    suffixIcon={<CaretDownOutlined
                                                        style={{
                                                            color: '#FF7506'
                                                        }}
                                                    />}
                                                    style={{
                                                        width: '162px',
                                                        height: '32px',
                                                        borderRadius: '8px',
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                    className='status'
                                                >
                                                    <Select.Option
                                                        value="Đang áp dụng"
                                                        key={1}
                                                        Title="Đang áp dụng"
                                                        style={{
                                                            height: '32px',
                                                            alignItems: 'center',
                                                            fontSize: '14px',
                                                            fontFamily: 'Montserrat-Regular'
                                                        }}
                                                    >
                                                        Đang áp dụng
                                                    </Select.Option>
                                                    <Select.Option
                                                        value="Tắt"
                                                        key={2}
                                                        Title="Tắt"
                                                        style={{
                                                            height: '32px',
                                                            alignItems: 'center',
                                                            fontSize: '14px',
                                                            fontFamily: 'Montserrat-Regular'
                                                        }}
                                                    >
                                                        Tắt
                                                    </Select.Option>
                                                </Select>
                                            </div>
                                            <div
                                                style={{
                                                    display: 'flex',
                                                }}>
                                                <div
                                                    style={{
                                                        height: '25px'
                                                    }}
                                                >
                                                    <Typography
                                                        style={{
                                                            color: 'red',
                                                            fontSize: '16px',
                                                            alignItems: 'center',
                                                            display: 'flex',
                                                            height: '20px',
                                                        }}>
                                                        *
                                                    </Typography>
                                                </div>
                                                <Typography
                                                    style={{
                                                        fontSize: '14px',
                                                        fontFamily: 'Montserrat-Italic'
                                                    }}
                                                >
                                                    là thông tin bắt buộc
                                                </Typography>
                                            </div>
                                        </div>
                                    </Modal>
                                    <Modal
                                        centered
                                        onCancel={handleCancelUpdate}
                                        width={600}
                                        closable={false}
                                        bodyStyle={{
                                            height: '363px',
                                            flexDirection: 'column',
                                            justifyContent: 'space-between',
                                            alignItems: 'center'
                                        }}
                                        title={
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'center',
                                                }}>
                                                <Typography
                                                    style={{
                                                        fontSize: '24px',
                                                        fontFamily: 'Montserrat-Bold',
                                                        color: '#1E0D03'
                                                    }}>
                                                    Cập nhật thông tin gói vé
                                                </Typography>
                                            </div>
                                        }
                                        open={popupUpdate}
                                        footer={[
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'center'
                                                }}>
                                                <Button
                                                    style={{
                                                        paddingInline: '50px',
                                                        height: '36px',
                                                        borderRadius: '8px',
                                                        display: 'flex',
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        borderColor: '#FF993C',
                                                        textAlign: 'center',
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '18px',
                                                            fontFamily: 'Montserrat-Bold',
                                                            color: '#FF993C'
                                                        }}>
                                                        Huỷ
                                                    </Typography>
                                                </Button>
                                                <Button
                                                    onClick={() => handleUpdateServicePack()}
                                                    style={{
                                                        paddingInline: '50px',
                                                        height: '36px',
                                                        borderRadius: '8px',
                                                        display: 'flex',
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        backgroundColor: '#FF993C',
                                                        textAlign: 'center',
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '18px',
                                                            fontFamily: 'Montserrat-Bold',
                                                            color: '#FFFFFF'
                                                        }}>
                                                        Lưu
                                                    </Typography>
                                                </Button>
                                            </div>
                                        ]}
                                    >
                                        <div>
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'space-between',
                                                    marginBottom: '12px',
                                                }}>
                                                <div>
                                                    <div
                                                        style={{
                                                            display: 'flex',
                                                        }}>
                                                        <Typography
                                                            style={{
                                                                fontSize: '16px',
                                                                fontFamily: 'Montserrat-SemiBold'
                                                            }}>
                                                            Mã sự kiện
                                                        </Typography>
                                                        <div
                                                            style={{
                                                                width: '25px',
                                                                height: '25px'
                                                            }}
                                                        >
                                                            <Typography
                                                                style={{
                                                                    color: 'red',
                                                                    fontSize: '16px',
                                                                    alignItems: 'center',
                                                                    display: 'flex',
                                                                    height: '20px',
                                                                    marginLeft: '2px'
                                                                }}>
                                                                *
                                                            </Typography>
                                                        </div>
                                                    </div>
                                                    <Input
                                                        onChange={(e) => setCodeUpdate(e.target.value)}
                                                        placeholder="Nhập mã sự kiện"
                                                        style={{
                                                            width: '194px',
                                                            height: '32px',
                                                            fontSize: '16px'
                                                        }}
                                                    />
                                                </div>
                                                <div
                                                    style={{
                                                        width: '290px'
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold'
                                                        }}>
                                                        Tên sự kiện
                                                    </Typography>
                                                    <Input
                                                        onChange={(e) => setNameUpdate(e.target.value)}
                                                        placeholder="Nhập tên sự kiện"
                                                        style={{
                                                            width: '290px',
                                                            height: '32px',
                                                            fontSize: '16px'
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'space-between',
                                                    marginBottom: '12px',
                                                }}>
                                                <div>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold'
                                                        }}>
                                                        Ngày áp dụng
                                                    </Typography>
                                                    <div
                                                        style={{
                                                            display: 'flex',
                                                        }}>
                                                        <DatePicker
                                                            placeholder='dd/mm/yy'
                                                            suffixIcon={<CalendarOutlined
                                                                style={{
                                                                    color: '#FF7506'
                                                                }} />}
                                                            value={selectedDateStartUpdate}
                                                            onChange={handleDateStartChangeUpdate}
                                                            style={{
                                                                width: '113px',
                                                                height: '32px'
                                                            }}
                                                        />
                                                        <TimePicker
                                                            placeholder='hh:mm:ss'
                                                            suffixIcon={<ClockCircleOutlined
                                                                style={{
                                                                    color: '#FF7506'
                                                                }} />}
                                                            value={selectedTimeStartUpdate}
                                                            onChange={handleTimeStartChangeUpdate}
                                                            style={{
                                                                width: '108px',
                                                                height: '32px'
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                                <div
                                                    style={{
                                                        width: '290px'
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold'
                                                        }}>
                                                        Ngày hết hạn
                                                    </Typography>
                                                    <div
                                                        style={{
                                                            display: 'flex',
                                                        }}>
                                                        <DatePicker
                                                            placeholder='dd/mm/yy'
                                                            suffixIcon={<CalendarOutlined
                                                                style={{
                                                                    color: '#FF7506'
                                                                }} />}
                                                            value={selectedDateEndUpdate}
                                                            onChange={handleDateEndChangeUpdate}
                                                            style={{
                                                                width: '113px',
                                                                height: '32px'
                                                            }}
                                                        />
                                                        <TimePicker
                                                            placeholder='hh:mm:ss'
                                                            suffixIcon={<ClockCircleOutlined
                                                                style={{
                                                                    color: '#FF7506'
                                                                }} />}
                                                            value={selectedTimeEndUpdate}
                                                            onChange={handleTimeEndChangeUpdate}
                                                            style={{
                                                                width: '108px',
                                                                height: '32px'
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <Typography
                                                    style={{
                                                        fontSize: '16px',
                                                        fontFamily: 'Montserrat-SemiBold'
                                                    }}>
                                                    Giá vé áp dụng
                                                </Typography>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        alignItems: 'center',
                                                        margin: '8px 0'
                                                    }}>
                                                    <Checkbox
                                                        checked={isCostCheckedUpdate}
                                                        onChange={handleCostCheckboxChangeUpdate}
                                                    >
                                                        <Typography
                                                            style={{
                                                                fontFamily: 'Montserrat-Medium',
                                                                fontSize: '16px',
                                                            }}
                                                        >
                                                            Vé lẻ (vnđ/vé) với giá
                                                        </Typography>
                                                    </Checkbox>
                                                    <Input
                                                        placeholder='Giá vé'
                                                        onChange={(e) => setCostUpdate(e.target.value)}
                                                        style={{
                                                            width: '117px',
                                                            height: '32px',
                                                            fontFamily: 'Nunito-Regular',
                                                            fontSize: '16px',
                                                            backgroundColor: '#F1F4F8'
                                                        }}>

                                                    </Input>
                                                    <div>
                                                        <Typography
                                                            style={{
                                                                fontFamily: 'Montserrat-Medium',
                                                                fontSize: '16px',
                                                                marginInline: '8px'
                                                            }}
                                                        >
                                                            / vé
                                                        </Typography>
                                                    </div>
                                                </div>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        alignItems: 'center',
                                                        margin: '8px 0'
                                                    }}>
                                                    <Checkbox
                                                        checked={isComboCheckedUpdate}
                                                        onChange={handleComboCheckboxChangeUpdate}
                                                    >
                                                        <Typography
                                                            style={{
                                                                fontFamily: 'Montserrat-Medium',
                                                                fontSize: '16px',
                                                            }}
                                                        >
                                                            Combo vé với giá
                                                        </Typography>
                                                    </Checkbox>
                                                    <Input
                                                        onChange={(e) => setComboUpdate(e.target.value)}
                                                        placeholder='Giá vé'
                                                        style={{
                                                            width: '117px',
                                                            height: '32px',
                                                            fontFamily: 'Nunito-Regular',
                                                            fontSize: '16px',
                                                            backgroundColor: '#F1F4F8'
                                                        }}>

                                                    </Input>
                                                    <div>
                                                        <Typography
                                                            style={{
                                                                fontFamily: 'Montserrat-Medium',
                                                                fontSize: '16px',
                                                                marginInline: '8px'
                                                            }}
                                                        >
                                                            /
                                                        </Typography>
                                                    </div>
                                                    <Input
                                                        onChange={(e) => setCostUpdate(e.target.value)}
                                                        placeholder='Giá vé'
                                                        style={{
                                                            width: '117px',
                                                            height: '32px',
                                                            fontFamily: 'Nunito-Regular',
                                                            fontSize: '16px',
                                                            backgroundColor: '#F1F4F8'
                                                        }}>

                                                    </Input>
                                                    <div>
                                                        <Typography
                                                            style={{
                                                                fontFamily: 'Montserrat-Medium',
                                                                fontSize: '16px',
                                                                marginInline: '8px'
                                                            }}
                                                        >
                                                            vé
                                                        </Typography>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                style={{
                                                    marginBottom: '12px',
                                                }}>
                                                <Typography
                                                    style={{
                                                        fontSize: '16px',
                                                        fontFamily: 'Montserrat-SemiBold'
                                                    }}>
                                                    Tình trạng
                                                </Typography>
                                                <Select
                                                    value={statusUpdate}
                                                    listHeight={180}
                                                    placeholder={'Chọn loại thiết bi'}
                                                    onChange={handleSelectStatusChangeUpdate}
                                                    suffixIcon={<CaretDownOutlined
                                                        style={{
                                                            color: '#FF7506'
                                                        }}
                                                    />}
                                                    style={{
                                                        width: '162px',
                                                        height: '32px',
                                                        borderRadius: '8px',
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                    className='status'
                                                >
                                                    <Select.Option
                                                        value="Đang áp dụng"
                                                        key={1}
                                                        Title="Đang áp dụng"
                                                        style={{
                                                            height: '32px',
                                                            alignItems: 'center',
                                                            fontSize: '14px',
                                                            fontFamily: 'Montserrat-Regular'
                                                        }}
                                                    >
                                                        Đang áp dụng
                                                    </Select.Option>
                                                    <Select.Option
                                                        value="Tắt"
                                                        key={2}
                                                        Title="Tắt"
                                                        style={{
                                                            height: '32px',
                                                            alignItems: 'center',
                                                            fontSize: '14px',
                                                            fontFamily: 'Montserrat-Regular'
                                                        }}
                                                    >
                                                        Tắt
                                                    </Select.Option>
                                                </Select>
                                            </div>
                                            <div
                                                style={{
                                                    display: 'flex',
                                                }}>
                                                <div
                                                    style={{
                                                        height: '25px'
                                                    }}
                                                >
                                                    <Typography
                                                        style={{
                                                            color: 'red',
                                                            fontSize: '16px',
                                                            alignItems: 'center',
                                                            display: 'flex',
                                                            height: '20px',
                                                        }}>
                                                        *
                                                    </Typography>
                                                </div>
                                                <Typography
                                                    style={{
                                                        fontSize: '14px',
                                                        fontFamily: 'Montserrat-Italic'
                                                    }}
                                                >
                                                    là thông tin bắt buộc
                                                </Typography>
                                            </div>
                                        </div>
                                    </Modal>
                                </div>
                            </div>
                            <div>
                                <Table
                                    pagination={{ pageSize: 9 }}
                                    dataSource={servicePacks ? servicePacks && servicePacks.filter(
                                        servicePack =>
                                            servicePack && servicePack.code && servicePack.code && servicePack.code.toLowerCase().includes(searchKeyword.toLowerCase())
                                    ) : servicePacks}
                                    columns={columns}
                                    rowClassName={(record, index) =>
                                        index % 2 === 0 ? "bg-white" : "bg-gray"
                                    }
                                    style={{
                                        height: '490px',
                                        marginTop: '4px'
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ServicePackContent