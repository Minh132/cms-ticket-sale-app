import React from 'react';
import logo from './logo.svg';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import Home from './pages/home'
import TicketMangement from './pages/ticketMangement'
import TicketCheck from './pages/ticketCheck'
import ServicePack from './pages/servicePack'
import { Provider } from 'react-redux';
import store from './features/store';
function App() {
  return (
    <Provider store={store}>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/ticket-management" element={<TicketMangement />} />
          <Route path="/ticket-check" element={<TicketCheck />} />
          <Route path="/service-pack" element={<ServicePack />} />
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
