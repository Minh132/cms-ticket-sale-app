import { Button, Input, Modal, Typography, Table, DatePicker, Radio, Checkbox, Space } from 'antd'
import {
    SearchOutlined,
    FilterOutlined,
    MoreOutlined,
    CalendarOutlined
} from '@ant-design/icons';
import { TicketFamily } from '../types/TicketFamily';
import React, {
    useState,
    useEffect
} from 'react'
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import {
    ticketFamilyCollection,
    updateTicketFamily
} from '../features/ticketFamily/ticketFamily';
import dayjs, { Dayjs } from 'dayjs';
import { useDispatch } from 'react-redux';
import type { RadioChangeEvent } from 'antd';

function TicketCheckContent() {
    const [startDate, setStartDate] = useState<string | null>(null);
    const [endDate, setEndDate] = useState<string | null>(null);

    const handleStartDateChange = (date: dayjs.Dayjs | null) => {
        const startDateAsDate = date ? date.format('DD/MM/YYYY') : null;
        setStartDate(startDateAsDate);
    };

    const handleEndDateChange = (date: dayjs.Dayjs | null) => {
        const endDateAsDate = date ? date.format('DD/MM/YYYY') : null;
        setEndDate(endDateAsDate);
    };
    const handleFilter = () => {
        // Lọc dữ liệu trong khoảng của dateUse và dateExport
        const filteredData = ticketManagementFamily.filter((ticket) => {
            const ticketDateUse = ticket.dateUse ? new Date(ticket.dateUse) : null;
            const ticketDateExport = ticket.dateExport ? new Date(ticket.dateExport) : null;

            const filterStartDate = startDate ? dayjs(startDate, 'DD/MM/YYYY').toDate() : null;
            const filterEndDate = endDate ? dayjs(endDate, 'DD/MM/YYYY').toDate() : null;

            return (
                (filterStartDate === null || ticketDateUse === null || ticketDateUse >= filterStartDate) &&
                (filterEndDate === null || ticketDateExport === null || ticketDateExport <= filterEndDate)
            );
        });

        // Sử dụng dữ liệu đã lọc
        console.log(filteredData);
    };
    const [selectedCheckIn, setSelectedCheckIn] = useState<string[]>([]);

    const handleCheckboxChange = (checkedValues: any) => {
        if (checkedValues.includes('Tất cả')) {
            setSelectedCheckIn(['Tất cả']);
        } else {
            setSelectedCheckIn(checkedValues.filter((value: any) => value !== 'all'));
        }
        console.log('checkbox', checkedValues)
    };
    const [checkStatus, setCheckStatus] = useState('Tất cả');
    const onChangeStatus = (e: RadioChangeEvent) => {
        console.log('radio checked', e.target.value);
        setCheckStatus(e.target.value);
    };
    const [searchKeyword, setSearchKeyword] = useState<string>('');
    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchKeyword(event.target.value);
    };
    const [popupUpdate, setPopupUpdate] = useState(false)
    const [id, setId] = useState("");

    const onChangeFilterTicketsUpdate = () => {
        popupChangeUpdate()
        console.log("Popup update showing");
    };
    const popupChangeUpdate = () => {
        setPopupUpdate(true)
    }
    const handleUpdate = (id: any) => {
        setId(id)
        onChangeFilterTicketsUpdate()
        console.log("Đã nhấp vào cập nhật cho ID vé:", id);
    };
    const handleCancelUpdate = () => {
        setPopupUpdate(false);
    };
    const [ticketManagementFamily, setTicketManagementFamily] = useState<TicketFamily[]>([]);
    const [selectedDateExport, setSelectedDateExport] = useState<Dayjs | null>(null);
    const [dateExport, setDateExport] = useState("");
    const [popup, setPopup] = useState(false)
    const popupChange = () => {
        setPopup(true)
    }
    const handleDateExportChangeUpdate = (date: Dayjs | null) => {
        setSelectedDateExport(date);
    };
    const onChangeFilterTickets = () => {
        popupChange()
        console.log("Popup showing");
    };
    const handleCancel = () => {
        setPopup(false);
    };
    const dispatch = useDispatch();

    useEffect(() => {
        const unsubscribe = onSnapshot(ticketFamilyCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const ticketFamilyData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setTicketManagementFamily(ticketFamilyData);
        });
        return () => {
            unsubscribe();
        };
    }, []);
    const handleUpdateTicketManagement = async () => {
        try {
            await dispatch(
                updateTicketFamily({
                    id: id,
                    docData: {
                        dateExport: dateExport
                    }
                }) as any
            );
            console.log('successfully update a new Service');
        } catch (error) {
            console.log('error:', error);
        }
    };
    useEffect(
        () => {
            const date = selectedDateExport ? selectedDateExport.format('DD/MM/YYYY') : '';
            const dateString = `${date}`;
            setDateExport(dateString);
            // if (!sttUpdate) {
            //     const maxSttUpdate = Math.max(...servicePacks.map((servicePack) => Number(servicePack.stt)), 0);
            //     setSttUpdate((maxSttUpdate + 1));
            //     console.log(sttUpdate)
            // } else {
            //     const maxSttUpdate = Math.max(...servicePacks.map((servicePack) => Number(servicePack.stt)), 0);
            //     setSttUpdate((maxSttUpdate + 1));
            //     console.log(stt)
            // }
        },
        [handleUpdateTicketManagement]
    )

    const columns = [
        {
            title: 'STT',
            dataIndex: 'stt',
            key: 'stt',
            render: (stt: number) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {stt}
                    </Typography>
                );
            },
        },
        {
            title: 'Số vé',
            dataIndex: 'number',
            key: 'number',
            render: (number: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {number}
                    </Typography>
                );
            },
        },
        {
            title: 'Ngày sử dụng',
            dataIndex: 'dateUse',
            key: 'dateUse',
            render: (dateUse: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {dateUse}
                    </Typography>
                );
            },
        },
        {
            title: 'Tên loại vé',
            dataIndex: 'name',
            key: 'name',
            render: (name: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {name}
                    </Typography>
                );
            },
        },
        {
            title: 'Cổng check-in',
            dataIndex: 'checkIn',
            key: 'checkIn',
            render: (checkIn: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {checkIn}
                    </Typography>
                );
            },
        },
        {
            title: ' ',
            dataIndex: 'checked',
            key: 'checked',
            render: (checked: string) => {
                return (
                    <>
                        {checked === 'false' ?
                            < Typography
                                style={{
                                    fontFamily: 'Montserrat-MediumItalic',
                                    fontSize: '14px',
                                    color: '#A5A8B1'
                                }}>
                                Chưa đối soát
                            </Typography>
                            :
                            <Typography
                                style={{
                                    fontFamily: 'Montserrat-MediumItalic',
                                    fontSize: '14px',
                                    color: '#FD5959'
                                }}>
                                Đã đối soát
                            </Typography>
                        }
                    </>

                );
            },
        },
    ]
    return (
        <div
            style={{
                display: 'flex',
            }}>
            <div
                style={{
                    margin: '0 20px',
                    display: 'flex',
                }}>
                <div
                    style={{
                        minHeight: '660px',
                        maxHeight: '660px',
                        borderRadius: '12px',
                        backgroundColor: '#FFFFFF',
                        padding: '24px',
                    }}>
                    <div
                        style={{
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            width: '840px',
                        }}>
                        <div>
                            <div>
                                <Typography
                                    style={{
                                        fontSize: '36px',
                                        fontFamily: 'Montserrat-Bold',
                                        color: '#1E0D03',
                                        lineHeight: 1
                                    }}>
                                    Đối soát vé
                                </Typography>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    width: '100%',
                                    alignItems: 'center',
                                }}>
                                <div>
                                    <Input
                                        onChange={handleSearch}
                                        className="custom-search-ticket"
                                        placeholder="Tìm bằng số vé"
                                        style={{
                                            width: '360px',
                                            height: '36px',
                                            background: '#F7F7F8'
                                        }}
                                        suffix={
                                            <SearchOutlined
                                                style={{
                                                    fontSize: '18px'
                                                }}
                                            />}
                                    />
                                </div>
                                <div
                                    style={{
                                        display: 'flex',
                                    }}>
                                    <Button
                                        style={{
                                            height: '36px',
                                            borderRadius: '8px',
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginInline: '4px',
                                            background: '#FF993C'
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '18px',
                                                fontFamily: 'Montserrat-Bold',
                                                color: '#FFFFFF'
                                            }}>
                                            Chốt đối soát
                                        </Typography>
                                    </Button>
                                </div>
                            </div>
                            <div>
                                <Modal
                                    centered
                                    onCancel={handleCancelUpdate}
                                    width={600}
                                    closable={false}
                                    bodyStyle={{
                                        flexDirection: 'column',
                                        justifyContent: 'space-between',
                                        alignItems: 'center'
                                    }}
                                    title={
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'center',
                                            }}>
                                            <Typography
                                                style={{
                                                    fontSize: '24px',
                                                    fontFamily: 'Montserrat-Bold',
                                                    color: '#1E0D03'
                                                }}>
                                                Đổi ngày sử dụng vé
                                            </Typography>
                                        </div>
                                    }
                                    open={popupUpdate}
                                    footer={[
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'center'
                                            }}>
                                            <Button
                                                style={{
                                                    paddingInline: '50px',
                                                    height: '36px',
                                                    borderRadius: '8px',
                                                    display: 'flex',
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    borderColor: '#FF993C',
                                                    textAlign: 'center',
                                                }}>
                                                <Typography
                                                    style={{
                                                        fontSize: '18px',
                                                        fontFamily: 'Montserrat-Bold',
                                                        color: '#FF993C'
                                                    }}>
                                                    Huỷ
                                                </Typography>
                                            </Button>
                                            <Button
                                                onClick={() => handleUpdateTicketManagement()}
                                                style={{
                                                    paddingInline: '50px',
                                                    height: '36px',
                                                    borderRadius: '8px',
                                                    display: 'flex',
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    backgroundColor: '#FF993C',
                                                    textAlign: 'center',
                                                }}>
                                                <Typography
                                                    style={{
                                                        fontSize: '18px',
                                                        fontFamily: 'Montserrat-Bold',
                                                        color: '#FFFFFF'
                                                    }}>
                                                    Lưu
                                                </Typography>
                                            </Button>
                                        </div>
                                    ]}
                                >
                                    {ticketManagementFamily.map((ticket) =>
                                        ticket.id === id ? <>
                                            <div>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        paddingBottom: '12px',
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold',
                                                            width: '160px'
                                                        }}>
                                                        Số vé
                                                    </Typography>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-Medium'
                                                        }}>
                                                        {ticket.number}
                                                    </Typography>
                                                </div>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        paddingBottom: '12px',
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold',
                                                            width: '160px'
                                                        }}>
                                                        Số vé
                                                    </Typography>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-Medium'
                                                        }}>
                                                        {ticket.name} - Gói gia đình
                                                    </Typography>
                                                </div>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        paddingBottom: '12px',
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold',
                                                            width: '160px'
                                                        }}>
                                                        Hạn sử dụng
                                                    </Typography>
                                                    <DatePicker
                                                        placeholder='dd/mm/yy'
                                                        suffixIcon={<CalendarOutlined
                                                            style={{
                                                                color: '#FF7506'
                                                            }} />}
                                                        value={selectedDateExport}
                                                        onChange={handleDateExportChangeUpdate}
                                                        style={{
                                                            width: '113px',
                                                            height: '32px'
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </> : ""
                                    )}
                                </Modal>
                            </div>
                            <div>
                                <Table
                                    pagination={{ pageSize: 9 }}
                                    dataSource={ticketManagementFamily ? ticketManagementFamily && ticketManagementFamily.filter(
                                        ticket =>
                                            ticket && ticket.number && ticket.number && ticket.number.toString().toLowerCase().includes(searchKeyword.toLowerCase())
                                    ) : ticketManagementFamily}
                                    columns={columns}
                                    rowClassName={(record, index) =>
                                        index % 2 === 0 ? "bg-white" : "bg-gray"
                                    }
                                    style={{
                                        height: '490px',
                                        marginTop: '4px'
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    style={{
                        minHeight: '660px',
                        maxHeight: '660px',
                        borderRadius: '12px',
                        backgroundColor: '#FFFFFF',
                        padding: '24px',
                        marginLeft: '20px'
                    }}>
                    <div
                        style={{
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            width: '320px',
                        }}>
                    </div>
                    <div>
                        <div>
                            <Typography
                                style={{
                                    fontSize: '24px',
                                    fontFamily: 'Montserrat-Bold',
                                    color: '#1E0D03',
                                    lineHeight: 1,
                                    marginBottom: '30px'
                                }}>
                                Lọc vé
                            </Typography>
                            <div
                                style={{
                                    marginBottom: '20px',
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Montserrat-SemiBold',
                                        width: '160px',
                                        marginRight: '10px'
                                    }}>
                                    Tình trạng đối soát
                                </Typography>
                                <Radio.Group onChange={onChangeStatus} value={checkStatus}>
                                    <Space direction="vertical">
                                        <Radio value={'Tất cả'}>Tất cả</Radio>
                                        <Radio value={'Đã đối soát'}>Đã đối soát</Radio>
                                        <Radio value={'Chưa đối soát'}>Chưa sử dụng</Radio>
                                    </Space>
                                </Radio.Group>
                            </div>
                            <div
                                style={{
                                    marginBottom: '20px',
                                    display: 'flex',
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Montserrat-SemiBold',
                                        width: '160px'
                                    }}>
                                    Loại vé
                                </Typography>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Montserrat-Medium',
                                        width: '160px'
                                    }}>
                                    Vé cổng
                                </Typography>
                            </div>
                            <div
                                style={{
                                    marginBottom: '20px',
                                    display: 'flex'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Montserrat-SemiBold',
                                        width: '160px'
                                    }}>
                                    Từ ngày
                                </Typography>
                                <DatePicker
                                    format="DD/MM/YYYY"
                                    placeholder='dd/mm/yy'
                                    suffixIcon={<CalendarOutlined
                                        style={{
                                            color: '#FF7506'
                                        }} />}
                                    onChange={handleStartDateChange}
                                    value={startDate ? dayjs(startDate) : null}
                                    style={{
                                        width: '150px',
                                        height: '32px'
                                    }}
                                />
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    marginBottom: '20px'
                                }}>
                                <Typography
                                    style={{
                                        fontSize: '16px',
                                        fontFamily: 'Montserrat-SemiBold',
                                        width: '160px'
                                    }}>
                                    Đến ngày
                                </Typography>
                                <DatePicker
                                    format="DD/MM/YYYY"
                                    placeholder='dd/mm/yy'
                                    suffixIcon={<CalendarOutlined
                                        style={{
                                            color: '#FF7506'
                                        }} />}
                                    onChange={handleEndDateChange}
                                    value={endDate ? dayjs(endDate) : null}
                                    style={{
                                        width: '150px',
                                        height: '32px'
                                    }}
                                />
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                }}>
                                <Button
                                    onClick={handleFilter}
                                    style={{
                                        paddingInline: '50px',
                                        height: '36px',
                                        borderRadius: '8px',
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderColor: '#FF993C',
                                        textAlign: 'center',
                                    }}>
                                    <Typography
                                        style={{
                                            fontSize: '18px',
                                            fontFamily: 'Montserrat-Bold',
                                            color: '#FF993C'
                                        }}>
                                        Lọc
                                    </Typography>
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default TicketCheckContent