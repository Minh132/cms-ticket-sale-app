import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
const firebaseConfig = {
    apiKey: "AIzaSyA_2kY9OxSvHysmPmjJjhYa5_rETsXmcFo",
    authDomain: "cms-ticket-eda2a.firebaseapp.com",
    projectId: "cms-ticket-eda2a",
    storageBucket: "cms-ticket-eda2a.appspot.com",
    messagingSenderId: "498262887370",
    appId: "1:498262887370:web:8b62d3097685e13c4c1d33",
    measurementId: "G-Q13FCL7M06"
};

export const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);