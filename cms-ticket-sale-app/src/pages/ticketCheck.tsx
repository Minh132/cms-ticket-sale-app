import { Layout, Space } from 'antd'
import React from 'react'
import SideBar from '../components/SideBar'
import TicketCheckContent from '../components/TicketCheckContent'
import Header from '../components/Header'
function ticketCheck() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <TicketCheckContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default ticketCheck
