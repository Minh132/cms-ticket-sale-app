import { Layout, Space } from 'antd'
import React from 'react'
import SideBar from '../components/SideBar'
import TicketManagementContent from '../components/TicketManagementContent'
import Header from '../components/Header'
function ticketMangement() {
    return (
        <Space
            direction="vertical"
            style={{ width: "100%", position: "absolute", top: 0 }}>
            <Layout>
                <SideBar />
                <Layout>
                    <Header />
                    <TicketManagementContent />
                </Layout>
            </Layout>
        </Space>
    )
}

export default ticketMangement