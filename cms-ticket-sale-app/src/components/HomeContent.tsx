import React, {
    useState
} from 'react';
import {
    Area
} from '@ant-design/plots';
import {
    PieChart,
    Pie,
    Cell,
    Tooltip
} from 'recharts';
import { RadioChangeEvent } from 'antd';
import { Dayjs } from 'dayjs';
import {
    Typography,
    DatePicker,
    Radio,
} from 'antd';
import type {
    DatePickerProps
} from 'antd';
import {
    CalendarOutlined,
} from '@ant-design/icons';
function HomeContent() {
    const [dateMode, setDateMode] = useState<'month' | 'date'>('month');
    const [selectedDate, setSelectedDate] = useState<Date | null>(null);

    const handleDateModeChange = (e: RadioChangeEvent) => {
        setDateMode(e.target.value as 'month' | 'date');
    };

    const handleDateChange = (date: Dayjs | null, dateString: string) => {
        setSelectedDate(date?.toDate() || null);
    };
    const renderExtraFooter = () => {
        return (
            <Radio.Group
                value={dateMode}
                onChange={handleDateModeChange}
                style={{ marginTop: 10 }}
            >
                <Radio value="month">Theo tuần</Radio>
                <Radio value="date">Theo ngày</Radio>
            </Radio.Group>
        );
    };
    const dataFamilyPack = [
        { name: 'B', value: 56024 },

        {
            name: 'A', value: 13568
        },
    ];
    const dataEventPackage = [
        { name: 'B', value: 28302 },

        {
            name: 'A', value: 30256
        },
    ];
    const COLORS = ['#4F75FF', '#FF8A48',];
    const day = [
        {
            "timePeriod": "29/03 - 04/04",
            "value": 150000000
        },
        {
            "timePeriod": "05/04 - 11/04",
            "value": 160000000
        },
        {
            "timePeriod": "12/01 - 18/04",
            "value": 230000000
        },
        {
            "timePeriod": "19/04 - 25/04",
            "value": 260000000
        },
        {
            "timePeriod": "26/04 - 02/05",
            "value": 190000000
        },
    ]
    const config = {
        data: day,
        xField: 'timePeriod',
        yField: 'value',
        xAxis: {
            range: [0, 1],
        },
        smooth: true,
        startOnZero: true,
        color: 'orange'
    };
    const handleChange = (value: { value: string; label: React.ReactNode }) => {
        console.log(value); // { value: "lucy", key: "lucy", label: "Lucy (101)" }
    }
    const onChange: DatePickerProps['onChange'] = (date, dateString) => {
        console.log(date, dateString);
    };
    return (
        <div
            style={{
                display: 'flex',
            }}>
            <div
                style={{
                    margin: '0 20px',
                }}>
                <div
                    style={{
                        minHeight: '660px',
                        maxHeight: '660px',
                        borderRadius: '12px',
                        backgroundColor: '#FFFFFF',
                        padding: '24px',
                    }}>
                    <div
                        style={{
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            width: '1240px',
                        }}>
                        <div>
                            <div>
                                <Typography
                                    style={{
                                        fontSize: '36px',
                                        fontFamily: 'Montserrat-Bold',
                                        color: '#1E0D03',
                                        lineHeight: 1
                                    }}>
                                    Thống kê
                                </Typography>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    width: '100%',
                                    alignItems: 'center',
                                }}>
                                <div>
                                    <Typography
                                        style={{
                                            fontSize: '18px',
                                            fontFamily: 'Montserrat-Semibold',
                                            color: '#1E0D03'
                                        }}>
                                        Doanh thu
                                    </Typography>
                                </div>
                                <div>
                                    <DatePicker
                                        suffixIcon={<CalendarOutlined style={{ color: '#FF7506' }} />}
                                        picker={dateMode === 'month' ? 'month' : 'date'}
                                        format={dateMode === 'month' ? 'MM, YYYY' : 'DD/MM/YYYY'}
                                        onChange={handleDateChange}
                                        style={{ width: '155px', height: '40px' }}
                                        renderExtraFooter={renderExtraFooter}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        style={{
                            height: '200px',
                            marginTop: '20px'
                        }}>
                        <Area {...config} />
                    </div>
                    <div
                        style={{
                            marginBottom: '10px',
                            marginTop: '10px'
                        }}>
                        <Typography
                            style={{
                                color: '#1E0D03',
                                opacity: '0.5'
                            }}>
                            Tổng doanh thu theo tuần
                        </Typography>
                        <div
                            style={{
                                display: 'flex',
                                marginBottom: '20px'
                            }}>
                            <Typography
                                style={{
                                    color: '#1E0D03',
                                    fontFamily: 'Montserrat-Bold',
                                    fontSize: '24px',
                                    display: 'flex',
                                    alignSelf: 'end',
                                    height: '32px',
                                }}>
                                525.145.000
                            </Typography>
                            <Typography
                                style={{
                                    color: '#1E0D03',
                                    display: 'flex',
                                    alignSelf: 'end',
                                }}>đồng
                            </Typography>
                        </div>
                        <div
                            style={{
                                display: 'flex',
                                justifyContent: 'space-between',
                            }}>
                            <DatePicker
                                suffixIcon={<CalendarOutlined style={{ color: '#FF7506' }} />}
                                picker={dateMode === 'month' ? 'month' : 'date'}
                                format={dateMode === 'month' ? 'MM, YYYY' : 'DD/MM/YYYY'}
                                onChange={handleDateChange}
                                style={{ width: '155px', height: '40px' }}
                                renderExtraFooter={renderExtraFooter}
                            />
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                }}>
                                <Typography
                                    style={{
                                        marginBottom: '10px',
                                        fontSize: '18px',
                                        fontFamily: 'Montserrat-Semibold'
                                    }}>
                                    Gói gia đình
                                </Typography>
                                <PieChart width={200} height={200}>
                                    <Pie
                                        data={dataFamilyPack}
                                        innerRadius={60}
                                        outerRadius={100}
                                        fill="#8884d8"
                                        dataKey="value"
                                        startAngle={-270}
                                    >
                                        {dataEventPackage.map((entry, index) => (
                                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                        ))}
                                    </Pie>
                                    <Tooltip />
                                </PieChart>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                }}>
                                <Typography
                                    style={{
                                        marginBottom: '10px',
                                        fontSize: '18px',
                                        fontFamily: 'Montserrat-Semibold'
                                    }}>
                                    Gói sự kiện
                                </Typography>
                                <PieChart width={200} height={200}>
                                    <Pie
                                        data={dataEventPackage}
                                        innerRadius={60}
                                        outerRadius={100}
                                        fill="#8884d8"
                                        dataKey="value"
                                        startAngle={-270}
                                    >
                                        {dataEventPackage.map((entry, index) => (
                                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                        ))}
                                    </Pie>
                                    <Tooltip />
                                </PieChart>
                            </div>
                            <div>
                                <div
                                    style={{
                                        display: 'flex',
                                        width: '160px',
                                        height: '24px',
                                        alignItems: 'center',
                                        gap: '8px',
                                    }}>
                                    <div
                                        style={{
                                            width: '44px',
                                            height: '20px',
                                            backgroundColor: '#4F75FF',
                                            borderRadius: '4px'
                                        }} />
                                    <Typography>
                                        Vé đã sử dụng
                                    </Typography>
                                </div>
                                <div
                                    style={{
                                        display: 'flex',
                                        width: '160px',
                                        height: '24px',
                                        alignItems: 'center',
                                        gap: '8px',
                                    }}>
                                    <div
                                        style={{
                                            width: '44px',
                                            height: '20px',
                                            backgroundColor: '#FF8A48',
                                            borderRadius: '4px'
                                        }} />
                                    <Typography>
                                        Vé chưa sử dụng
                                    </Typography>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default HomeContent