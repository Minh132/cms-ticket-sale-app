import { Button, Input, Modal, Typography, Table, DatePicker, Radio, Checkbox } from 'antd'
import {
    SearchOutlined,
    FilterOutlined,
    MoreOutlined,
    CalendarOutlined
} from '@ant-design/icons';
import { TicketFamily } from '../types/TicketFamily';
import React, {
    useState,
    useEffect
} from 'react'
import {
    DocumentData,
    onSnapshot,
    QuerySnapshot
} from "firebase/firestore";
import {
    ticketFamilyCollection,
    updateTicketFamily
} from '../features/ticketFamily/ticketFamily';
import dayjs, { Dayjs } from 'dayjs';
import { useDispatch } from 'react-redux';
import type { RadioChangeEvent } from 'antd';

function TicketManagementContent() {
    const [startDate, setStartDate] = useState<string | null>(null);
    const [endDate, setEndDate] = useState<string | null>(null);

    const handleStartDateChange = (date: dayjs.Dayjs | null) => {
        const startDateAsDate = date ? date.format('DD/MM/YYYY') : null;
        setStartDate(startDateAsDate);
        console.log(startDateAsDate)
    };

    const handleEndDateChange = (date: dayjs.Dayjs | null) => {
        const endDateAsDate = date ? date.format('DD/MM/YYYY') : null;
        setEndDate(endDateAsDate);
        console.log(endDateAsDate)
    };
    const handleFilter = () => {
        // Lọc dữ liệu trong khoảng của dateUse và dateExport
        const filteredData = ticketManagementFamily.filter((ticket) => {
            const ticketDateUse = ticket.dateUse ? new Date(ticket.dateUse) : null;
            const ticketDateExport = ticket.dateExport ? new Date(ticket.dateExport) : null;

            const filterStartDate = startDate ? dayjs(startDate, 'DD/MM/YYYY').toDate() : null;
            const filterEndDate = endDate ? dayjs(endDate, 'DD/MM/YYYY').toDate() : null;

            return (
                (filterStartDate === null || ticketDateUse === null || ticketDateUse >= filterStartDate) &&
                (filterEndDate === null || ticketDateExport === null || ticketDateExport <= filterEndDate)
            );
        });

        // Sử dụng dữ liệu đã lọc
        console.log(filteredData);
    };
    const [selectedCheckIn, setSelectedCheckIn] = useState<string[]>([]);

    const handleCheckboxChange = (checkedValues: any) => {
        if (checkedValues.includes('Tất cả')) {
            setSelectedCheckIn(['Tất cả']);
        } else {
            setSelectedCheckIn(checkedValues.filter((value: any) => value !== 'all'));
        }
        console.log('checkbox', checkedValues)
    };
    const [checkStatus, setCheckStatus] = useState('Tất cả');
    const onChangeStatus = (e: RadioChangeEvent) => {
        console.log('radio checked', e.target.value);
        setCheckStatus(e.target.value);
    };
    const [searchKeyword, setSearchKeyword] = useState<string>('');
    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchKeyword(event.target.value);
    };
    const [popupUpdate, setPopupUpdate] = useState(false)
    const [id, setId] = useState("");

    const onChangeFilterTicketsUpdate = () => {
        popupChangeUpdate()
        console.log("Popup update showing");
    };
    const popupChangeUpdate = () => {
        setPopupUpdate(true)
    }
    const handleUpdate = (id: any) => {
        setId(id)
        onChangeFilterTicketsUpdate()
        console.log("Đã nhấp vào cập nhật cho ID vé:", id);
    };
    const handleCancelUpdate = () => {
        setPopupUpdate(false);
    };
    const [ticketManagementFamily, setTicketManagementFamily] = useState<TicketFamily[]>([]);
    const [selectedDateExport, setSelectedDateExport] = useState<Dayjs | null>(null);
    const [dateExport, setDateExport] = useState("");
    const [popup, setPopup] = useState(false)
    const popupChange = () => {
        setPopup(true)
    }
    const handleDateExportChangeUpdate = (date: Dayjs | null) => {
        setSelectedDateExport(date);
    };
    const onChangeFilterTickets = () => {
        popupChange()
        console.log("Popup showing");
    };
    const handleCancel = () => {
        setPopup(false);
    };
    const dispatch = useDispatch();

    useEffect(() => {
        const unsubscribe = onSnapshot(ticketFamilyCollection, (snapshot: QuerySnapshot<DocumentData>) => {
            const ticketFamilyData = snapshot.docs.map((doc) => {
                return {
                    id: doc.id,
                    ...doc.data(),
                };
            });
            setTicketManagementFamily(ticketFamilyData);
        });
        return () => {
            unsubscribe();
        };
    }, []);
    const handleUpdateTicketManagement = async () => {
        try {
            await dispatch(
                updateTicketFamily({
                    id: id,
                    docData: {
                        dateExport: dateExport
                    }
                }) as any
            );
            console.log('successfully update a new Service');
        } catch (error) {
            console.log('error:', error);
        }
    };
    useEffect(
        () => {
            const date = selectedDateExport ? selectedDateExport.format('DD/MM/YYYY') : '';
            const dateString = `${date}`;
            setDateExport(dateString);
            // if (!sttUpdate) {
            //     const maxSttUpdate = Math.max(...servicePacks.map((servicePack) => Number(servicePack.stt)), 0);
            //     setSttUpdate((maxSttUpdate + 1));
            //     console.log(sttUpdate)
            // } else {
            //     const maxSttUpdate = Math.max(...servicePacks.map((servicePack) => Number(servicePack.stt)), 0);
            //     setSttUpdate((maxSttUpdate + 1));
            //     console.log(stt)
            // }
        },
        [handleUpdateTicketManagement]
    )

    const columns = [
        {
            title: 'STT',
            dataIndex: 'stt',
            key: 'stt',
            render: (stt: number) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {stt}
                    </Typography>
                );
            },
        },
        {
            title: 'Booking code',
            dataIndex: 'bookingCode',
            key: 'bookingCode',
            render: (bookingCode: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {bookingCode}
                    </Typography>
                );
            },
        },
        {
            title: 'Số vé',
            dataIndex: 'number',
            key: 'number',
            render: (number: number) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {number}
                    </Typography>
                );
            },
        },
        {
            title: 'Tình trạng sử dụng',
            dataIndex: 'status',
            key: 'status',
            render: (status: string) => {
                return (
                    <>
                        {status === 'Đã sử dụng'
                            ?
                            <div
                                style={{
                                    display: 'inline-flex',
                                    alignItems: 'center',
                                    backgroundColor: '#EAF1F8',
                                    border: '1px solid #919DBA',
                                    borderRadius: '4px',
                                    paddingInline: '8px',
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#919DBA',
                                        borderRadius: '100%',
                                        marginRight: '4px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Montserrat-Medium',
                                        fontSize: '14px',
                                        color: '#919DBA'
                                    }}>
                                    Đã sử dụng
                                </Typography>
                            </div>
                            : ""
                        }
                        {status === 'Chưa sử dụng'
                            ?
                            <div
                                style={{
                                    display: 'inline-flex',
                                    alignItems: 'center',
                                    backgroundColor: '#DEF7E0',
                                    border: '1px solid #03AC00',
                                    borderRadius: '4px',
                                    paddingInline: '8px',
                                }}>
                                <div
                                    style={{
                                        width: '6px',
                                        height: '6px',
                                        backgroundColor: '#03AC00',
                                        borderRadius: '100%',
                                        marginRight: '4px'
                                    }}></div>
                                <Typography
                                    style={{
                                        fontFamily: 'Montserrat-Medium',
                                        fontSize: '14px',
                                        color: '#03AC00'
                                    }}>
                                    Chưa sử dụng
                                </Typography>
                            </div>
                            : ""
                        }
                        {
                            status === 'Hết hạn'
                                ?
                                <div
                                    style={{
                                        display: 'inline-flex',
                                        alignItems: 'center',
                                        backgroundColor: '#F8EBE8',
                                        border: '1px solid #FD5959',
                                        borderRadius: '4px',
                                        paddingInline: '8px',
                                    }}>
                                    <div
                                        style={{
                                            width: '6px',
                                            height: '6px',
                                            backgroundColor: '#FD5959',
                                            borderRadius: '100%',
                                            marginRight: '4px'
                                        }}></div>
                                    <Typography
                                        style={{
                                            fontFamily: 'Montserrat-Medium',
                                            fontSize: '14px',
                                            color: '#FD5959'
                                        }}>
                                        Hết hạn
                                    </Typography>
                                </div>
                                : ""
                        }
                    </>
                );
            },
        },
        {
            title: 'Ngày sử dụng',
            dataIndex: 'dateUse',
            key: 'dateUse',
            render: (dateUse: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {dateUse}
                    </Typography>
                );
            },
        },
        {
            title: 'Ngày xuất vé',
            dataIndex: 'dateExport',
            key: 'dateExport',
            render: (dateExport: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {dateExport}
                    </Typography>
                );
            },
        },
        {
            title: 'Cổng check-in',
            dataIndex: 'checkIn',
            key: 'checkIn',
            render: (checkIn: string) => {
                return (
                    <Typography
                        style={{
                            fontFamily: 'Montserrat-Medium',
                            fontSize: '14px'
                        }}>
                        {checkIn}
                    </Typography>
                );
            },
        },
        {
            title: ' ',
            dataIndex: 'update',
            key: 'update',
            render: (_: any, record: TicketFamily) => {
                const { id } = record; // Giả sử ID của gói được lưu trữ trong thuộc tính 'id'

                return (
                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                        }}
                        onClick={() => handleUpdate(id)} // Truyền ID của gói vào hàm handleUpdate
                    >
                        <MoreOutlined />
                    </div>
                );
            },
        },
    ]
    return (
        <div
            style={{
                display: 'flex',
            }}>
            <div
                style={{
                    margin: '0 20px',
                }}>
                <div
                    style={{
                        minHeight: '660px',
                        maxHeight: '660px',
                        borderRadius: '12px',
                        backgroundColor: '#FFFFFF',
                        padding: '24px',
                    }}>
                    <div
                        style={{
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            width: '1240px',
                        }}>
                        <div>
                            <div>
                                <Typography
                                    style={{
                                        fontSize: '36px',
                                        fontFamily: 'Montserrat-Bold',
                                        color: '#1E0D03',
                                        lineHeight: 1
                                    }}>
                                    Danh sách vé
                                </Typography>
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    width: '100%',
                                    alignItems: 'center',
                                }}>
                                <div>
                                    <Input
                                        onChange={handleSearch}
                                        className="custom-search-ticket"
                                        placeholder="Tìm bằng booking code"
                                        style={{
                                            width: '360px',
                                            height: '36px',
                                            background: '#F7F7F8'
                                        }}
                                        suffix={
                                            <SearchOutlined
                                                style={{
                                                    fontSize: '18px'
                                                }}
                                            />}
                                    />
                                </div>
                                <div
                                    style={{
                                        display: 'flex',
                                    }}>
                                    <Button
                                        onClick={() => onChangeFilterTickets()}
                                        style={{
                                            height: '36px',
                                            borderRadius: '8px',
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginInline: '4px',
                                            borderColor: '#FF993C'
                                        }}>
                                        <FilterOutlined
                                            style={{
                                                color: '#FF993C',
                                                fontSize: '18px'
                                            }} />
                                        <Typography
                                            style={{
                                                fontSize: '18px',
                                                fontFamily: 'Montserrat-Bold',
                                                color: '#FF993C',
                                                marginLeft: '10px'
                                            }}>
                                            Lọc vé
                                        </Typography>
                                    </Button>
                                    <Modal
                                        centered
                                        onCancel={handleCancel}
                                        width={500}
                                        closable={false}
                                        style={{
                                            flexDirection: 'column',
                                            justifyContent: 'space-between',
                                            alignItems: 'center'
                                        }}
                                        title={
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'center',
                                                }}>
                                                <Typography
                                                    style={{
                                                        fontSize: '24px',
                                                        fontFamily: 'Montserrat-Bold',
                                                        color: '#1E0D03'
                                                    }}>
                                                    Lọc vé
                                                </Typography>
                                            </div>
                                        }
                                        open={popup}
                                        footer={[
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'center'
                                                }}>
                                                <Button
                                                    onClick={handleFilter}
                                                    style={{
                                                        paddingInline: '50px',
                                                        height: '36px',
                                                        borderRadius: '8px',
                                                        display: 'flex',
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        borderColor: '#FF993C',
                                                        textAlign: 'center',
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '18px',
                                                            fontFamily: 'Montserrat-Bold',
                                                            color: '#FF993C'
                                                        }}>
                                                        Lọc
                                                    </Typography>
                                                </Button>
                                            </div>
                                        ]}
                                    >
                                        <div>
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    paddingBottom: '12px',
                                                }}>
                                                <div>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold',
                                                            width: '200px'
                                                        }}>
                                                        Từ ngày
                                                    </Typography>
                                                    <DatePicker
                                                        format="DD/MM/YYYY"
                                                        placeholder='dd/mm/yy'
                                                        suffixIcon={<CalendarOutlined
                                                            style={{
                                                                color: '#FF7506'
                                                            }} />}
                                                        onChange={handleStartDateChange}
                                                        value={startDate ? dayjs(startDate) : null}
                                                        style={{
                                                            width: '113px',
                                                            height: '32px'
                                                        }}
                                                    />
                                                </div>
                                                <div>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold',
                                                            width: '200px'
                                                        }}>
                                                        Đến ngày
                                                    </Typography>
                                                    <DatePicker
                                                        format="DD/MM/YYYY"
                                                        placeholder='dd/mm/yy'
                                                        suffixIcon={<CalendarOutlined
                                                            style={{
                                                                color: '#FF7506'
                                                            }} />}
                                                        onChange={handleEndDateChange}
                                                        value={endDate ? dayjs(endDate) : null}
                                                        style={{
                                                            width: '113px',
                                                            height: '32px'
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                            <div>
                                                <Typography
                                                    style={{
                                                        fontSize: '16px',
                                                        fontFamily: 'Montserrat-SemiBold',
                                                        width: '200px'
                                                    }}>
                                                    Tình trạng sử dụng
                                                </Typography>
                                                <Radio.Group onChange={onChangeStatus} value={checkStatus}>
                                                    <Radio value={'Tất cả'}>Tất cả</Radio>
                                                    <Radio value={'Đã sử dụng'}>Đã sử dụng</Radio>
                                                    <Radio value={'Chưa sử dụng'}>Chưa sử dụng</Radio>
                                                    <Radio value={'Hết hạn'}>Hết hạn</Radio>
                                                </Radio.Group>
                                            </div>
                                            <div>
                                                <Typography
                                                    style={{
                                                        fontSize: '16px',
                                                        fontFamily: 'Montserrat-SemiBold',
                                                        width: '200px'
                                                    }}>
                                                    Cổng Check - in
                                                </Typography>
                                                <Checkbox.Group
                                                    value={selectedCheckIn}
                                                    onChange={handleCheckboxChange}
                                                >
                                                    <Checkbox value={'Tất cả'}>Tất cả</Checkbox>
                                                    <Checkbox value={'Cổng 1'}>Cổng 1</Checkbox>
                                                    <Checkbox value={'Cổng 2'}>Cổng 2</Checkbox>
                                                    <Checkbox value={'Cổng 3'}>Cổng 3</Checkbox>
                                                    <Checkbox value={'Cổng 4'}>Cổng 4</Checkbox>
                                                    <Checkbox value={'Cổng 5'}>Cổng 5</Checkbox>
                                                </Checkbox.Group>
                                            </div>
                                        </div>
                                    </Modal>
                                    <Button
                                        style={{
                                            height: '36px',
                                            borderRadius: '8px',
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginInline: '4px',
                                            borderColor: '#FF993C'
                                        }}>
                                        <Typography
                                            style={{
                                                fontSize: '18px',
                                                fontFamily: 'Montserrat-Bold',
                                                color: '#FF993C'
                                            }}>
                                            Xuất file (.csv)
                                        </Typography>
                                    </Button>
                                </div>
                            </div>
                            <div>
                                <Modal
                                    centered
                                    onCancel={handleCancelUpdate}
                                    width={600}
                                    closable={false}
                                    bodyStyle={{
                                        flexDirection: 'column',
                                        justifyContent: 'space-between',
                                        alignItems: 'center'
                                    }}
                                    title={
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'center',
                                            }}>
                                            <Typography
                                                style={{
                                                    fontSize: '24px',
                                                    fontFamily: 'Montserrat-Bold',
                                                    color: '#1E0D03'
                                                }}>
                                                Đổi ngày sử dụng vé
                                            </Typography>
                                        </div>
                                    }
                                    open={popupUpdate}
                                    footer={[
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'center'
                                            }}>
                                            <Button
                                                style={{
                                                    paddingInline: '50px',
                                                    height: '36px',
                                                    borderRadius: '8px',
                                                    display: 'flex',
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    borderColor: '#FF993C',
                                                    textAlign: 'center',
                                                }}>
                                                <Typography
                                                    style={{
                                                        fontSize: '18px',
                                                        fontFamily: 'Montserrat-Bold',
                                                        color: '#FF993C'
                                                    }}>
                                                    Huỷ
                                                </Typography>
                                            </Button>
                                            <Button
                                                onClick={() => handleUpdateTicketManagement()}
                                                style={{
                                                    paddingInline: '50px',
                                                    height: '36px',
                                                    borderRadius: '8px',
                                                    display: 'flex',
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    backgroundColor: '#FF993C',
                                                    textAlign: 'center',
                                                }}>
                                                <Typography
                                                    style={{
                                                        fontSize: '18px',
                                                        fontFamily: 'Montserrat-Bold',
                                                        color: '#FFFFFF'
                                                    }}>
                                                    Lưu
                                                </Typography>
                                            </Button>
                                        </div>
                                    ]}
                                >
                                    {ticketManagementFamily.map((ticket) =>
                                        ticket.id === id ? <>
                                            <div>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        paddingBottom: '12px',
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold',
                                                            width: '160px'
                                                        }}>
                                                        Số vé
                                                    </Typography>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-Medium'
                                                        }}>
                                                        {ticket.number}
                                                    </Typography>
                                                </div>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        paddingBottom: '12px',
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold',
                                                            width: '160px'
                                                        }}>
                                                        Số vé
                                                    </Typography>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-Medium'
                                                        }}>
                                                        {ticket.name} - Gói gia đình
                                                    </Typography>
                                                </div>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        paddingBottom: '12px',
                                                    }}>
                                                    <Typography
                                                        style={{
                                                            fontSize: '16px',
                                                            fontFamily: 'Montserrat-SemiBold',
                                                            width: '160px'
                                                        }}>
                                                        Hạn sử dụng
                                                    </Typography>
                                                    <DatePicker
                                                        placeholder='dd/mm/yy'
                                                        suffixIcon={<CalendarOutlined
                                                            style={{
                                                                color: '#FF7506'
                                                            }} />}
                                                        value={selectedDateExport}
                                                        onChange={handleDateExportChangeUpdate}
                                                        style={{
                                                            width: '113px',
                                                            height: '32px'
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </> : ""
                                    )}
                                </Modal>
                            </div>
                            <div>
                                <Table
                                    pagination={{ pageSize: 12 }}
                                    dataSource={ticketManagementFamily ? ticketManagementFamily && ticketManagementFamily.filter(
                                        ticket =>
                                            ticket && ticket.bookingCode && ticket.bookingCode && ticket.bookingCode.toLowerCase().includes(searchKeyword.toLowerCase())
                                    ) : ticketManagementFamily}
                                    columns={columns}
                                    rowClassName={(record, index) =>
                                        index % 2 === 0 ? "bg-white" : "bg-gray"
                                    }
                                    style={{
                                        height: '490px',
                                        marginTop: '4px'
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default TicketManagementContent