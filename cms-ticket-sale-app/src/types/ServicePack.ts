export interface ServicePack {
    stt?: number;
    id?: string;
    code?: string
    name?: string;
    dayStart?: string;
    dayEnd?: string;
    cost?: string;
    combo?: string;
    status?: string;
}