import { configureStore } from '@reduxjs/toolkit';
import servicePack from './servicePack/servicePack';
import ticketFamily from './ticketFamily/ticketFamily';
const store = configureStore({
    reducer: {
        servicePack: servicePack,
        ticketFamily: ticketFamily,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;