import React from 'react'
import { Image, Input } from 'antd';
import {
    SearchOutlined,
    MailOutlined,
    BellOutlined
} from '@ant-design/icons';
function Header() {
    return (
        <div
            style={{
                margin: '10px 20px',
                display: 'flex',
                justifyContent: 'space-between'
            }}>
            <Input
                className="custom-search"
                placeholder="Search"
                style={{
                    width: '360px',
                    height: '36px',
                    background: '#EDE6E6'
                }}
                suffix={
                    <SearchOutlined
                        style={{
                            fontSize: '18px'
                        }}
                    />}
            />
            <div
                style={{
                    alignItems: 'center',
                    display: 'flex',
                }}>
                <MailOutlined
                    style={{
                        marginRight: '12px',
                        color: '#1E0D03'
                    }} />
                <BellOutlined
                    style={{
                        marginRight: '12px',
                        color: '#1E0D03'
                    }} />
                <Image
                    preview={false}
                    src={'https://upload.wikimedia.org/wikipedia/commons/0/0c/E-girl.png'}
                    style={{
                        marginRight: '12px',
                        borderRadius: '100%',
                        width: '24px',
                        height: '24px'
                    }} />
            </div>
        </div>
    )
}

export default Header