export interface TicketEvent {
    stt?: number;
    id?: string;
    bookingCode?: string;
    number?: number;
    status?: string;
    dateUse?: string;
    dateExport?: string;
    checkIn?: string;
    name?: string;
    checked?: string;
    nameEvent?: string;
}